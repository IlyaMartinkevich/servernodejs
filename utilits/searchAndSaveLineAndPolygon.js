/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */
module.exports = (dataJsonParse) => {
    let pointsLineArray = []
    let pointsPolygonArray = []
    let flag = true
    dataJsonParse.forEach(function (data, index) {
        if (data['isBindingLine'] == 'false' && flag) {
            pointsLineArray.push([data['coordinateX'], data['coordinateY']])
        } else {
            if (flag) {
                pointsLineArray.push([data['coordinateX'], data['coordinateY']])
                flag = false
            }
            pointsPolygonArray.push([data['coordinateX'], data['coordinateY']])
        }
    })

    return  {
        pointsLineArray: pointsLineArray,
        pointsPolygonArray: pointsPolygonArray,
    }
}



