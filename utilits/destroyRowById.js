/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */


// удалим если есть из бд

const destroyRowByIDLC  = (Model, idLC) =>{

    Model.destroy({
        where: {
            id_lc: idLC
        }
    });

}

module.exports =  destroyRowByIDLC