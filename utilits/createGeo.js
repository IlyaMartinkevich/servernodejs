/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

const destroyRowByIDLC = require('../utilits/destroyRowById')
const db = require('../models/index')

const createGeo = (dataLines, dataPolygon, IdLC) => {

    if (dataLines.length > 1) {
        destroyRowByIDLC(db.Privazka, IdLC)

        const lines = {
            type: 'LineString', coordinates: dataLines,
            crs: {type: 'name', properties: {name: db.config.EPSG_32635}}
        };
        db.Privazka.create({
            geom: lines,
            //coordinates: JSON.stringify(dataLines).toString(),
            id_lc: IdLC
        });

    }

    if (dataPolygon.length > 3) { // по правильному 2 , но ставим три
        destroyRowByIDLC(db.Lesoseka, IdLC)

        const polygon = {
            type: 'Polygon', coordinates:
                [dataPolygon],
            crs: {type: 'name', properties: {name: db.config.EPSG_32635}}
        };
        db.Lesoseka.create({
            geom: polygon,
            //coordinates: JSON.stringify(dataPolygon).toString(),
            id_lc: IdLC
        });
    }
}


const createRowLcPrivazkaAndLcLesoseka = (IdLC) => {
        destroyRowByIDLC(db.Privazka, IdLC)
        db.Privazka.create({
            id_lc: IdLC
        });
    destroyRowByIDLC(db.Lesoseka, IdLC)
        db.Lesoseka.create({
            id_lc: IdLC
        });
}

module.exports = createGeo