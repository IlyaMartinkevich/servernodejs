/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */
var cors = require('cors')
const https = require("https")
const http = require("http")
const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const morgan = require("morgan")
const config = require('./config/config')


const db = require("./models/index")(config);
db.sequelize.sync()

app.use(cors())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.get('/', (req, res) => {
    res.end(`<div> 
<h1>App NodeJs</h1>
</div>`)
})

app.use(morgan("dev"))
app.use('/api/save', require('./routes/save.routes'))
app.use('/api/get', require('./routes/get.routes'))
app.use('/api/update', require('./routes/update.routes'))

app.listen(config.PORT, (req, res) => {
    console.log("Express server is running at port no " + config.HOST + ":" + config.PORT)

})
/*
https.createServer(app).listen(PORT_HTTPS,
    () => console.log("Express server is running at port no https://127.0.0.1:" + PORT_HTTPS))

http.createServer(app).listen(PORT_HTTP,
    () => console.log("Express server is running at port no http://127.0.0.1:" + PORT_HTTP))
*/

