/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = {
    portHTTPS: 3028,
    portHTTP: 3020,
    urlConnectBd: "postgres://postgres:bkmz111@localhost:5432/LC_BSTU",
    PORT: process.env.PORT || 4210,
    HOST: process.env.HOST || `http://127.0.0.1`,
    EPSG_32635: 'EPSG:32635'

};