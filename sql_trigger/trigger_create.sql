/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

/* Триггер для обновления привязки  */
CREATE FUNCTION public.update_geometryPrivazka()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS
$BODY$
DECLARE
    stringBuffer varchar;
    rowBuffer
                 lc_geo%rowtype;
BEGIN
    FOR rowBuffer IN
        select *
        from lc_geo
        where is_binding_line is false
          and id_lc = NEW.id_lc
        union
        (select * from lc_geo where is_binding_line is true and id_lc = NEW.id_lc limit 1)
        LOOP
            stringBuffer := concat_ws(', ', stringBuffer, concat(rowBuffer.coordinate_x, ' ', rowBuffer.coordinate_y));
        END LOOP;
    UPDATE lc_privazka
    SET geom = ST_GeomFromText('LINESTRING(' || stringBuffer || ')', 32635)
    WHERE id_lc = NEW.id_lc;
    RETURN NEW;
END;
$BODY$;
ALTER FUNCTION public.update_geometryPrivazka()
    OWNER TO postgres;



/* Триггер для обновления лесосеки  */

CREATE FUNCTION public.update_geometryLesoseka()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS
$BODY$
DECLARE
    stringBuffer varchar;
    rowBuffer
                 lc_geo%rowtype;
BEGIN
    FOR rowBuffer IN
        select *
        from lc_geo
        where is_binding_line is true
          and id_lc = NEW.id_lc
        order by id
        LOOP
            stringBuffer := concat_ws(', ', stringBuffer, concat(rowBuffer.coordinate_x, ' ', rowBuffer.coordinate_y));
        END LOOP;
    RAISE
        NOTICE '(%)', stringBuffer;

    UPDATE lc_lesoseka
    SET geom = ST_GeomFromText('POLYGON((' || stringBuffer || '))', 32635)
    WHERE id_lc = NEW.id_lc;
    RETURN NEW;
END;
$BODY$;
ALTER FUNCTION public.update_geometryLesoseka()
    OWNER TO postgres;

