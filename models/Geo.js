/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return  sequelize.define('lc_geo', {
        id: {
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        id_lc:{
            type: Sequelize.STRING,
            foreignKey: true,
        },
        azimuth: Sequelize.STRING,
        position_point: Sequelize.INTEGER,
        coordinate_x: Sequelize.STRING,
        coordinate_y: Sequelize.STRING,
        direction_angle: Sequelize.STRING,
        horizontal_distance: Sequelize.STRING,
        rhumb: Sequelize.STRING,
        slant_distance: Sequelize.STRING,
        vertical_angle: Sequelize.STRING,
        is_binding_line: Sequelize.BOOLEAN,
    },{
        timestamps:false,
        tableName:'lc_geo',
    });
}