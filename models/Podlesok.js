/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return Podlesok = sequelize.define('lc_podlesok', {
        id: {
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        id_lc:{
            type: Sequelize.STRING,
            foreignKey:true,
        },
        code_porod: Sequelize.INTEGER,
        tree_count: Sequelize.INTEGER,
        tree_middle_height: Sequelize.STRING,
    },{
        timestamps:false,
        tableName:'lc_podlesok',
    })
}