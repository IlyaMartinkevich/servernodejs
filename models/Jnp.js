/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return sequelize.define('lc_jnp', {
        id: {
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        id_lc:{
            type: Sequelize.STRING,
            foreignKey:true,
        },
        groundcover_kind_1: Sequelize.INTEGER,
        groundcover_kind_2: Sequelize.INTEGER,
        groundcover_kind_3: Sequelize.INTEGER,
    },{
        timestamps:false,
        tableName:'lc_jnp',
    })
}