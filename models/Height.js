/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return sequelize.define('lc_height', {
        id: {
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        id_lc: {
            type: Sequelize.STRING,
            foreignKey:true,
        },
        tier: Sequelize.INTEGER,
        generation: Sequelize.INTEGER,
        code_porod:Sequelize.INTEGER,
        diameter: Sequelize.DOUBLE,
        height: Sequelize.DOUBLE,
        initial_height: Sequelize.DOUBLE,
    },{
        timestamps:false,
        tableName:'lc_height',
    });
}
