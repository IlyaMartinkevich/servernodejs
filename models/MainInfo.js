/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return sequelize.define('lc_info', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
        },
        id_lc: {
            type: Sequelize.STRING,
            primaryKey: true,
        },
        code_lesxoz: Sequelize.INTEGER,
        code_plxo: Sequelize.INTEGER,
        code_lestichestva: Sequelize.INTEGER,
        number_vided: Sequelize.INTEGER,
        number_kvartala: Sequelize.INTEGER,
        number_region: Sequelize.INTEGER,
        number_lesoseka: Sequelize.INTEGER,
        age: Sequelize.STRING,


    }, {
        timestamps: false,
        tableName: 'lc_info',
    })
}
