/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return sequelize.define('lc_podrost', {
        id: {
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        id_lc: {
            type:Sequelize.STRING,
            foreignKey:true
        },
        age: Sequelize.INTEGER,
        bad_tree_count: Sequelize.INTEGER,
        breed: Sequelize.INTEGER,
        dry_tree_count: Sequelize.INTEGER,
        good_tree_count: Sequelize.INTEGER,
        height_category: Sequelize.INTEGER,

    },{
        timestamps:false,
        tableName:'lc_podrost',
    })
}
