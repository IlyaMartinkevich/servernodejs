/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return sequelize.define('lc_lesoseka', {
        id: {
            type:Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement:true,
        },
        id_lc: {
            type: Sequelize.STRING,
            primaryKey: true,
        },
        geom: Sequelize.GEOMETRY,
        //coordinates: Sequelize.STRING,
    },{
        timestamps:false,
        tableName:'lc_lesoseka',
    })
}