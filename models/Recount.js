/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

module.exports = (sequelize, Sequelize) => {
    return sequelize.define('lc_recount', {
        id: {
            type:Sequelize.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        id_lc: {
            type: Sequelize.STRING,
            foreignKey:true
        },

        step: Sequelize.INTEGER,
        code_porod: Sequelize.INTEGER,
        count_drov: Sequelize.INTEGER,
        count_del: Sequelize.INTEGER,
        count_syx: Sequelize.INTEGER,
        count_trees_not_felling: Sequelize.INTEGER,
        tier: Sequelize.INTEGER,
    },{
        timestamps:false,
        tableName:'lc_recount',
    })
}
