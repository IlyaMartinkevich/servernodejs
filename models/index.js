/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */
//const colors = require('colors');

module.exports = (config) => {
    const {Sequelize} = require('sequelize')


    //Local connect
    const sequelize = new Sequelize(config.urlConnectBd);

    //Remote connect
   /* const sequelize = new Sequelize(process.env.DATABASE_URL, {
        dialectOptions: {
            ssl: {
                require: true,
                rejectUnauthorized: false
            }
        }
    })*/

    const db = {
        Sequelize: Sequelize,
        sequelize: sequelize,
        config: config,
        MainInfo: require("../models/MainInfo")(sequelize, Sequelize),
        Geo: require("../models/Geo")(sequelize, Sequelize),
        Recount: require("../models/Recount")(sequelize, Sequelize),
        Height: require("../models/Height")(sequelize, Sequelize),
        Podlesok: require("../models/Podlesok")(sequelize, Sequelize),
        Podrostok: require("../models/Podrostok")(sequelize, Sequelize),
        Jnp: require("../models/Jnp")(sequelize, Sequelize),
        Privazka: require("../models/Privazka")(sequelize, Sequelize),
        Lesoseka: require("../models/Lesoseka")(sequelize, Sequelize),
    }


    db.MainInfo.hasMany(db.Geo, {foreignKey: 'id_lc'})
    db.MainInfo.hasMany(db.Recount, {foreignKey: 'id_lc'})
    db.MainInfo.hasMany(db.Height, {foreignKey: 'id_lc'})
    db.MainInfo.hasMany(db.Podlesok, {foreignKey: 'id_lc'})
    db.MainInfo.hasMany(db.Podrostok, {foreignKey: 'id_lc'})
    db.MainInfo.hasMany(db.Jnp, {foreignKey: 'id_lc'})

    db.MainInfo.hasOne(db.Privazka, {foreignKey: 'id_lc'})
    db.MainInfo.hasOne(db.Lesoseka, {foreignKey: 'id_lc'})


    module.exports = db
    return db

}