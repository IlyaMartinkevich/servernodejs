/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

const {Router} = require('express')
const router = Router()
const db = require('../models/index')

router.put('/geo', async (req, res) => {
    try {
        console.log(req.body)
        const IdLC = req.body[0]['id_lc']
        req.body.forEach(function (data, index) {


            db.Geo.update({
                    coordinate_x: data['coordinateX'],
                    coordinate_y: data['coordinateY'],
                    is_binding_line: data['isBindingLine']
                }
                , {
                    where: {
                        id_lc: data['id_lc'],
                        id: data['id']
                    }
                })
        })
       /* const searchAndSaveLineAndPolygon = require('../utilits/searchAndSaveLineAndPolygon')(req.body)

        const createGeo = require('../utilits/createGeo')
        createGeo(searchAndSaveLineAndPolygon.pointsLineArray, searchAndSaveLineAndPolygon.pointsPolygonArray, IdLC)*/


        res.status(200).json({
            message: "Data edit",
        })



    } catch (e) {
        res.status(500).json({message: "Error server " + e.message})
        console.log("Error " + e.message)
    }
})


module.exports = router