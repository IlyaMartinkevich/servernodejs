/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

const {Router} = require('express')
const router = Router()
const db = require('../models/index')
const destroyRowByIDLC = require('../utilits/destroyRowById')

// /api/save/podrostok
router.post('/podrostok', async (req, res) => {
    try {
        const {data, IdLC} = req.body
        destroyRowByIDLC(db.Podrostok, IdLC)
        let dataJsonParse = JSON.parse(data)
        dataJsonParse.forEach(function (data, index) {
            db.Podrostok.create({
                age: data['age'],
                breed: data['breed'],
                bad_tree_count: data['badTreeCount'],
                dry_tree_count: data['dryTreeCount'],
                good_tree_count: data['goodTreeCount'],
                height_category: data['heightCategory'],
                id_lc: IdLC,
            })
        })
        res.status(200).json({
            message: "Data podrostok add",
        })

    } catch (e) {
        res.status(500).json({message: "Error server " + e.message})
    }
})

router.post('/podlesok', async (req, res) => {
    try {
        const {data, IdLC} = req.body
        destroyRowByIDLC(db.Podlesok, IdLC)
        let dataJsonParse = JSON.parse(data)


        dataJsonParse.forEach(function (data, index) {
            db.Podlesok.create({
                code_porod: data['breed'],
                tree_count: data['treeCount'],
                tree_middle_height: data['treeMiddleHeight'],
                id_lc: IdLC,
            })
        })


        res.status(200).json({
            message: "Data podlesok add",
        })
    } catch (e) {
        res.status(500).json({message: "Error server: " + e.message})
    }
})


router.post('/geo', async (req, res) => {
    try {
        const {data, IdLC} = req.body
        destroyRowByIDLC(db.Geo, IdLC)

        let dataJsonParse = JSON.parse(data)

        let positionPoint = 0
        dataJsonParse.forEach(function (data, index) {

            db.Geo.create({
                azimuth: data['azimuth'],
                coordinate_x: data['coordinateX'],
                coordinate_y: data['coordinateY'],
                position_point: positionPoint,
                direction_angle: data['directionAngle'],
                horizontal_distance: data['horizontalDistance'],
                rhumb: data['rhumb'],
                slant_distance: data['slantDistance'],
                vertical_angle: data['verticalAngle'],
                is_binding_line: data['isBindingLine'],
                id_lc: IdLC,
            })
            positionPoint++

        })

        const searchAndSaveLineAndPolygon = require('../utilits/searchAndSaveLineAndPolygon')(dataJsonParse)
        const createGeo = require('../utilits/createGeo')
        createGeo(searchAndSaveLineAndPolygon.pointsLineArray, searchAndSaveLineAndPolygon.pointsPolygonArray, IdLC)

        /*const createRowLcPrivazkaAndLcLesoseka = require('../utilits/createGeo')
        createRowLcPrivazkaAndLcLesoseka(IdLC)*/


        res.status(200).json({
            message: "Data geodezik add",
        })
    } catch (e) {
        console.log("Error " + e.message)
        res.status(500).json({message: "Error server " + e.message})
    }
})


router.post('/recount', async (req, res) => {
    try {
        const {data, IdLC} = req.body
        destroyRowByIDLC(db.Recount, IdLC)

        var dataJsonParse = JSON.parse(data)


        dataJsonParse.forEach(function (data, index) {

            db.Recount.create({
                step: data['step'],
                count_drov: data['countDrov'],
                code_porod:data['codePorod'],
                count_del: data['countDel'],
                count_syx: data['countSyx'],
                count_trees_not_felling: data['countTreesNotFelling'],
                tier: data['tier'],
                id_lc: data['idLc']
            })

        })
        res.status(200).json({
            message: "Data Recount add",
        })
    } catch (e) {
        res.status(500).json({message: "Error server " + e.message})
    }
})


router.post('/height', async (req, res) => {
    try {
        const {data, IdLC} = req.body
        destroyRowByIDLC(db.Height, IdLC)

        const dataJsonParse = JSON.parse(data)
        let codePorod = 0;
        dataJsonParse.forEach(function (data, index) {
            if(data['code_porod'] == null ||
                data['code_porod'] =='null'){
                codePorod = 0
            }else {
                codePorod = data['code_porod']
            }
            db.Height.create({
                tier: data['tier'],
                generation: data['generation'],
                code_porod: codePorod,
                diameter: data['diameter'],
                height: data['height'],
                initial_height: data['initialHeight'],
                id_lc: IdLC
            })

        })
        res.status(200).json({
            message: "Data Height add",
        })
    } catch (e) {
        res.status(500).json({message: "Error server " + e.message})
    }
})


router.post('/jnp', async (req, res) => {
    try {
        const {data, IdLC} = req.body
        var dataJsonParse = JSON.parse(data)
        destroyRowByIDLC(db.Jnp, IdLC)


        dataJsonParse.forEach(function (data, index) {
            db.Jnp.create({
                groundcover_kind_1: data['groundcover_kind_1'],
                groundcover_kind_2: data['groundcover_kind_2'],
                groundcover_kind_3: data['groundcover_kind_3'],
                id_lc: IdLC
            })
        })

        res.status(200).json({
            message: "Data Jnp add",
        })
    } catch (e) {
        res.status(500).json({message: "Error server " + e.message})
    }
})


router.post('/info', (req, res) => {
    try {

        const {dataAll, dataPloat, IdLC,ageLc} = req.body
        var dataJsonParseAll = JSON.parse(dataAll)
        var dataJsonParsePloat = JSON.parse(dataPloat)
        db.MainInfo.findOne({where: {id_lc: IdLC}}).then((data) => {
            if (data == null) {
                db.MainInfo.create({
                    code_lesxoz: dataJsonParseAll['forestFarmCode'],
                    code_plxo: dataJsonParseAll['forestryProductionAssociationCode'],
                    code_lestichestva: dataJsonParseAll['forestryCode'],
                    number_vided: dataJsonParseAll['landAllocationNo'],
                    number_kvartala: dataJsonParseAll['quarterNo'],
                    number_region: dataJsonParseAll['regionCode'],
                    number_lesoseka: dataJsonParsePloat['plotNo'],
                    age: ageLc,
                    id_lc: IdLC
                })
            } else {
                db.MainInfo.update({
                    code_lesxoz: dataJsonParseAll['forestFarmCode'],
                    code_plxo: dataJsonParseAll['forestryProductionAssociationCode'],
                    code_lestichestva: dataJsonParseAll['forestryCode'],
                    number_vided: dataJsonParseAll['landAllocationNo'],
                    number_kvartala: dataJsonParseAll['quarterNo'],
                    number_region: dataJsonParseAll['regionCode'],
                    number_lesoseka: dataJsonParsePloat['plotNo'],
                    x_center: dataJsonParsePloat['xCenter'],
                    y_scenter: dataJsonParsePloat['yCenter'],
                    age: ageLc,
                    id_lc: IdLC
                }, {where: {id_lc: IdLC}})
            }
            res.status(200).json({
                message: "Data MainInfo add",
                result: true
            })
        }).catch((err) => {
            res.status(500).json({
                message: "Error server " + err.message,
                result: false
            })
        })

    } catch (e) {
        res.status(500).json({message: "Error server " + e.message,
            result: false})

    }
})




module.exports = router
