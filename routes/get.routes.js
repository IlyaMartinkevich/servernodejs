/**
 * author: Martinkevich Ilya
 * email: ilia1mail2martinkevi4@gmail.com
 */

const {Router} = require('express')
const router = Router()
const db = require('../models/index')

router.get('/geo/:idLc', async (req, res) => {
    try {

        db.Geo.findAll({
            where: {
                id_lc: req.params['idLc']
            }
        }).then((data) => {
            if (data.length == 0) {
                res.status(400).json({
                    message: "Data not get",
                })
            } else {
                res.status(200).json({
                    data: data,
                    message: "Data get",
                })
            }


        }).catch(() => {
            res.status(500).json({message: "Error server " + e.message})
        })


    } catch (e) {
        res.status(500).json({message: "Error server " + e.message})
    }
})


module.exports = router